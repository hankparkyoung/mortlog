import React from "react"
import internal from "stream"

export {}

declare global {
  type Unit = {
    id: number
    cost: number
    name: string
    traits: Trait[]
  }
  type Trait = {
    id: number
    name: string
    units: Unit[]
    intervals: number[]
  }
  // type GameUnit = {
  //   id: number
  //   unit: Unit
  //   game: Game
  // }
  // type Game = {
  //   id: number
  //   gameUnits: GameUnit[]
  //   placement: number
  //   activeTraits: Trait[]
  //   notes: string
  // }
}
