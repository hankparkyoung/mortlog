import prisma from '../lib/prisma';
import Link from 'next/link';

import { UnitTile } from '../components/unitTile';

export default async function Page() {
  const log = await prisma.game.findMany({
    include: {
      gameUnits: {
        select: {
          unit: {
            select: {
              id: true,
              name: true,
              cost: true,
            },
          },
        },
      },
    },
  });
  const logReversed = log.reverse();

  const gameCard = ({placement, notes, gameUnits}) => {
    return (
      <div className='m-2 p-2 w-80 border-2 border-black'>
        <h2>Placement: {placement}</h2>
        <div className='flex'>
          <div className='flex flex-col'>
            <p>Units: </p>
            {gameUnits.map(({unit}) => (
              <UnitTile key={unit.id} unit={unit} />
            ))}
          </div>
        </div>
        
        <p>Notes: </p>
        <span>{notes}</span>
      </div>
    )
  }

  return (
    <>
      <h1>All Games</h1>
      <Link href='/create'>Create</Link>
      {logReversed.map((game) => (
        <div key={game.id}>
          {gameCard(game)}
        </div>
      ))}
    </>
  )
}