import prisma from '../../lib/prisma';
import Link from 'next/link';
import CreateLogForm from '../../components/createLogForm';

export default async function Page() {
  const unitList = await prisma.unit.findMany({
    include: {
      traits: true,
    }
  });
  const traitList = await prisma.trait.findMany();
  // await prisma.headliner.findMany();

  const formData = {
    units: unitList,
    traits: traitList,
  }

  async function handleSubmit(formData) {
    'use server'
    const placementInt = parseInt(formData.placement);
    const units = formData.selectedUnits.map(unit => {
      return { 
        unitId: unit.id,
      }
    });
    const game = await prisma.game.create({
      data: {
        placement: placementInt,
        notes: formData.notes,
        gameUnits: {
          createMany: {
            data: units,
          },
        },
      },
      include: {
        gameUnits: true,
      },
    });

    console.log(game);
  } 

  return (
    <>
      <Link href='/'>All Games</Link>
      <CreateLogForm
        initialData={formData}
        handleSubmit={handleSubmit}
      />
    </>
  );
}