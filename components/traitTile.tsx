'use client'

interface TraitTileProps {
  trait: Trait
  tier?: number
}

export const TraitTile: React.FC<TraitTileProps> = ({ trait, tier }) => {
  const traitColors = {
    1: 'bg-slate-200',
    2: 'bg-amber-200',
  };

  return (
    <p
      className={`
        flex items-center justify-center
        w-32 h-8 m-1
        border-black border-2 rounded-2xl
        ${traitColors[tier]}
        text-xs
    `}
    >{trait.name}</p>
  )
}