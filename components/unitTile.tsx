'use client'

interface UnitTileProps {
  unit: Unit
  tileHandler?: (unit: Unit) => void
}

export const UnitTile: React.FC<UnitTileProps> = ({ unit, tileHandler }) => {
  const unitColors = {
    1: 'bg-zinc-300',
    2: 'bg-emerald-200',
    3: 'bg-sky-200',
    4: 'bg-violet-300',
    5: 'bg-amber-200',
  }

  const unitHovers = {
    1: 'hover:bg-zinc-400',
    2: 'hover:bg-emerald-400',
    3: 'hover:bg-sky-400',
    4: 'hover:bg-violet-400',
    5: 'hover:bg-amber-400',
  }

  return (
    <button
      type='button'
      className={`
        w-32 h-8 m-1
        border-black border-2
        text-xs
        ${unitColors[unit.cost]}
        ${unitHovers[unit.cost]}
      `}
      onClick={(e) => {
        e.preventDefault();
        return tileHandler ? tileHandler(unit) : console.log('no handle');
      }}
    >
      {unit.name}
    </button>
  )
}