'use client'
import Link from 'next/link';
import { useState } from 'react';
import { UnitTile } from './unitTile';
import { TraitTile } from './traitTile';

export default function CreateLogForm({ initialData, handleSubmit }) {

  const { units, traits } = initialData;
  const unitCosts = [1, 2, 3, 4, 5]; // could move this
  const [formData, setFormData] = useState({
    selectedUnits: [],
    activeTraits: [],
    headliner: {
      unit: null,
      trait: null,
    },
    placement: '',
    notes: '',
  });
  const [headlinerTraitList, setHeadlinerTraitList] = useState([]);

  const handleAddUnit = (unit: Unit) => {
    setFormData((prevData) => {
      const updatedUnits = [...prevData.selectedUnits, unit];
      findActiveTraits(updatedUnits, formData.headliner.trait);
      return { ...prevData, selectedUnits: updatedUnits };
    });
  };

  const handleRemoveUnit = (unit: Unit) => {
    setFormData((prevData) => {
      const updatedUnits = [...prevData.selectedUnits];
      const updatedHeadliner = prevData.headliner;
      if (unit === prevData.headliner.unit) {
        updatedHeadliner.unit = null;
        updatedHeadliner.trait = null;
        setHeadlinerTraitList([]);
      }
      updatedUnits.splice(updatedUnits.indexOf(unit), 1);
      findActiveTraits(updatedUnits);
      return { ...prevData, selectedUnits: updatedUnits, headliner: updatedHeadliner };
    })
  }

  const findActiveTraits = (units: Unit[], headlinerTrait: Trait = null) => {
    const presentTraits = {};
    const uniqueUnits: Unit[] = Array.from(new Set(units));

    uniqueUnits.forEach((unit: Unit) => {
      unit.traits.forEach((trait: Trait) => {
        if (presentTraits[trait.name]) {
          presentTraits[trait.name]++;
        } else {
          presentTraits[trait.name] = 1;
        };
      });
    });
    if (headlinerTrait) {
      presentTraits[headlinerTrait.name] ++;
    }

    const activeTraits = traits.filter((trait: Trait) => {
      return Object.keys(presentTraits).includes(trait.name) &&
        presentTraits[trait.name] >= trait.intervals[0];
    });

    setFormData((prevData) => {
      return { ...prevData, activeTraits: activeTraits }
    });
  };

  const handleHeadlinerUnitChange = (unit: Unit) => {
    const selectedUnit = units.find((u: Unit) => u.name === unit.name);
    const newHeadliner = {
      ...formData.headliner,
      unit: selectedUnit,
    };
    setHeadlinerTraitList(unit.traits);
    setFormData((prevData) => {
      return { ...prevData, headliner: newHeadliner };
    });
    setHeadlinerTraitList(selectedUnit.traits);
  };

  const handleHeadlinerTraitChange = (trait: Trait) => {
    const selectedTrait = traits.find((t: Trait) => t.name === trait.name);
    const newHeadliner = {
      ...formData.headliner,
      trait: selectedTrait,
    };
    findActiveTraits(formData.selectedUnits, trait)
    setFormData((prevData) => {
      return { ...prevData, headliner: newHeadliner };
    });
  };

  const handlePlacement = (placement: string) => {
    setFormData((prevData) => {
      return { ...prevData, placement: placement };
    });
  };

  const handleNotes = (notes: string) => {
    setFormData((prevData) => {
      return { ...prevData, notes: notes };
    });
  };

  // async function handleSubmit(formData) {
  //   const placementInt = parseInt(formData.placement);
  //   const game = await prisma.game.create({
  //     data: {
  //       placement: placementInt,
  //       notes: formData.notes,
  //       gameUnits: {
  //         createMany: {
  //           data: formData.selectedUnits,
  //         },
  //       },
  //       activeTraits: formData.activeTraits,
  //     },
  //     include: {
  //       gameUnits: true,
  //     },
  //   });

  //   console.log(game);
  // }

  return (
    <form className={`
      flex w-100 m-2 p-2 border-2 border-black
      space-x-5
    `}>
      <div>
        <h2 className='ml-1'>Add Units:</h2>
        <div className='flex'>
          {unitCosts.map((cost) => (
            <div key={cost} className={`flex flex-col`}>
              {units.filter((u: Unit) => u.cost === cost).map((unit: Unit) => (
                <UnitTile
                  key={unit.id}
                  unit={unit}
                  tileHandler={handleAddUnit}
                />
              ))}
            </div>
          ))}
        </div>
      </div>
      <div className='w-36'>
        <h2 className='ml-1'>Selected Units:</h2>
        <div className='flex flex-col'>
          {formData.selectedUnits.map((unit: Unit, i: number) => (
            <div key={i} className='flex'>
              <UnitTile
                unit={unit}
                tileHandler={handleHeadlinerUnitChange}
              />
              <button
                className={`
                  flex items-center justify-center
                  w-4 h-4 mt-3
                  border-black border-2
                  bg-red-100
                  hover:bg-red-400
                  text-xs`}
                type='button'
                onClick={() => handleRemoveUnit(unit)}
              >x</button>
            </div>
          ))}
        </div>
      </div>
      <div className='w-40'>
        <h2 className='ml-1'>Active Traits:</h2>
        <div className='flex flex-col'>
          {formData.activeTraits.map((trait: Trait) => (
            <TraitTile
              key={trait.id}
              trait={trait}
              tier={1}
            />
          ))}
        </div>
      </div>
      <div className='w-40'>
        <div className='h-20'>
          <h2 className='ml-1'>Headliner Unit:</h2>
          {formData.headliner.unit ? <UnitTile
            unit={formData.headliner.unit}
            tileHandler={() => {}}
          /> : null}
        </div>
        <h2 className='ml-1'>Headliner Trait:</h2>
        <div className='flex flex-col h-20'>
          {headlinerTraitList.map((trait: Trait) => (
            <div key={trait.id} className='flex'>
              <TraitTile
                trait={trait}
                tier={formData.headliner.trait?.id === trait.id ? 2 : 1}
              />
              <button
                className={`
                  flex items-center justify-center
                  w-4 h-4 mt-3
                  border-black border-2
                  bg-emerald-200
                  hover:bg-emerald-400
                  text-xs`}
                type='button'
                onClick={() => handleHeadlinerTraitChange(trait)}
              >o</button>
            </div>
          ))}
        </div>
        <h2 className='ml-1'>Placement:</h2>
        <input
          className='border-2 border-black'
          value={formData.placement}
          onChange={(e) => handlePlacement(e.target.value)}
        />
        <h2 className='ml-1'>Notes:</h2>
        <input
          className='border-2 border-black'
          value={formData.notes}
          onChange={(e) => handleNotes(e.target.value)}
        />
      </div>
      <Link href='/'>
        <button
          type='button'
          onClick={() => handleSubmit(formData)}
        >
          Submit?
        </button>
      </Link>
    </form>
  )
}
